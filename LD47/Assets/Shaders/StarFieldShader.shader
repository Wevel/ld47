﻿Shader "Sprites/StarFieldShader"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        
        _Offset ("View Offset", Vector) = (0,0,0,0)

        _NoiseStrength ("Noise Strength", FLOAT) = 1
        _NoiseFloor ("Noise Floor", FLOAT) = 0
        _NoiseTex ("Noise Texture", 2D) = "white" {}

        _BaseStarColour ("Base Star Colour", Color) = (0.2, 0.3, 0.9, 1)
        _SecondaryStarColour ("Secondary Colour", Color) = (1, 0.25, 1, 1)
        _HaloStarColour ("Halo Colour", Color) = (0.2, 0.2, 0.1, 1)

        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

        SubShader
        {
            Tags
            {
                "Queue" = "Transparent"
                "IgnoreProjector" = "True"
                "RenderType" = "Transparent"
                "PreviewType" = "Plane"
                "CanUseSpriteAtlas" = "True"
            }

            Cull Off
            Lighting Off
            ZWrite Off
            Blend One OneMinusSrcAlpha

            Pass
            {
            CGPROGRAM
                #pragma vertex Vert
                #pragma fragment Frag
                #pragma target 2.0
                #pragma multi_compile_instancing
                #pragma multi_compile_local _ PIXELSNAP_ON
                #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
                
                #include "UnityCG.cginc"

                #ifdef UNITY_INSTANCING_ENABLED

                UNITY_INSTANCING_BUFFER_START (PerDrawSprite)
                        // SpriteRenderer.Color while Non-Batched/Instanced.
                        UNITY_DEFINE_INSTANCED_PROP (fixed4, unity_SpriteRendererColorArray)
                        // this could be smaller but that's how bit each entry is regardless of type
                        UNITY_DEFINE_INSTANCED_PROP (fixed2, unity_SpriteFlipArray)
                    UNITY_INSTANCING_BUFFER_END (PerDrawSprite)

                    #define _RendererColor  UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteRendererColorArray)
                    #define _Flip           UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteFlipArray)

                #endif // instancing

                CBUFFER_START (UnityPerDrawSprite)
                #ifndef UNITY_INSTANCING_ENABLED
                    fixed4 _RendererColor;
                    fixed2 _Flip;
                #endif
                    float _EnableExternalAlpha;
                CBUFFER_END

                struct appdata_t
                {
                    float4 vertex   : POSITION;
                    float4 color    : COLOR;
                    float2 texcoord : TEXCOORD0;
                    UNITY_VERTEX_INPUT_INSTANCE_ID
                };

                struct v2f
                {
                    float4 vertex   : SV_POSITION;
                    fixed4 color : COLOR;
                    float2 texcoord : TEXCOORD0;
                    float4 worldPos : TEXCOORD1;
                    UNITY_VERTEX_OUTPUT_STEREO
                };

                inline float4 UnityFlipSprite (in float3 pos, in fixed2 flip)
                {
                    return float4(pos.xy * flip, pos.z, 1.0);
                }
               
                v2f Vert (appdata_t IN)
                {
                    v2f OUT;

                    UNITY_SETUP_INSTANCE_ID (IN);
                    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO (OUT);

                    OUT.vertex = UnityFlipSprite (IN.vertex, _Flip);
                    OUT.vertex = UnityObjectToClipPos (OUT.vertex);
                    OUT.texcoord = IN.texcoord;
                    OUT.worldPos = UnityFlipSprite (IN.vertex, _Flip);
                    OUT.worldPos = mul (unity_ObjectToWorld, OUT.worldPos);
                    OUT.color = IN.color * _RendererColor;

                    #ifdef PIXELSNAP_ON
                    OUT.vertex = UnityPixelSnap (OUT.vertex);
                    #endif

                    return OUT;
                }

                sampler2D _MainTex;
                sampler2D _AlphaTex;

                fixed4 SampleSpriteTexture (float2 uv)
                {
                    fixed4 color = tex2D (_MainTex, uv);

                #if ETC1_EXTERNAL_ALPHA
                    fixed4 alpha = tex2D (_AlphaTex, uv);
                    color.a = lerp (color.a, alpha.r, _EnableExternalAlpha);
                #endif

                    return color;
                }

                sampler2D _NoiseTex;
                float4 _NoiseTex_ST;

                // Star field is a modified version of the code found here:
                // https://timcoster.com/2020/07/18/unity-shadergraph-starfield-tutorial/
                float2x2 Rot (float a) 
                {
                    float s = sin (a), c = cos (a);
                    return float2x2(c, -s, s, c);
                }

                float Star (float2 uv, float flare)
                {
                    float d = length (uv);
                    float m = 0.05 / d;

                    float rays = max (0.0, 1.0 - abs (uv.x * uv.y * 1000.0));
                    m += rays * flare;
                    //uv = mul (uv, Rot (3.1415 / 4.0));
                    rays = max (0.0, 1.0 - abs (uv.x * uv.y * 1000.0));
                    m += rays * 0.3 * flare;

                    m *= smoothstep (1.0, 0.2, d);
                    return m;
                }

                float Hash21 (float2 p) 
                {
                    p = frac (p * float2(123.34, 456.21));
                    p += dot (p, p + 45.32);
                    return frac (p.x * p.y);
                }

                float4 _BaseStarColour;
                float4 _SecondaryStarColour;
                float4 _HaloStarColour;

                float3 StarLayer (float2 uv) 
                {
                    float3 col = float3(0.0, 0.0, 0.0);

                    float2 gv = frac (uv) - 0.5;
                    float2 id = floor (uv);

                    for (float y = -1.0; y <= 1.0; y++) {
                        for (float x = -1.0; x <= 1.0; x++) {
                            float2 offs = float2(x, y);

                            float n = Hash21 (id + offs); // random between 0 and 1
                            float size = frac (n * 3.4532);

                            float star = lerp (0, Star (gv - offs - float2(n, frac (n * 34.0)) + 0.5, smoothstep (0.9, 1.0, size) * 0.3), step (0.6, size));

                            float3 color = sin (_BaseStarColour.rgb * frac (n * 2345.2) * 123.2) * 0.5 + 0.5;
                            color = color * float3(_SecondaryStarColour.r, _SecondaryStarColour.g, _SecondaryStarColour.b + size) + _HaloStarColour.rgb;
                            col += star * size * color;
                        }
                    }
                    return col;
                }
            
                float _NoiseStrength;
                float _NoiseFloor;

                float2 _Offset;

                fixed4 Frag (v2f IN) : SV_Target
                {
                    fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;

                    float2 uv = IN.worldPos.xy + _Offset;

                    fixed4 noise = tex2D (_NoiseTex, uv * _NoiseTex_ST.xy + _NoiseTex_ST.zw);

                    float3 col = float3(0.0, 0.0, 0.0);
                    col += StarLayer (uv * 4 / 10);
                    col += StarLayer (uv * 0.1 / 10 + 823.5);
                    col += StarLayer (uv * 0.05 / 10 + 453.2);

                    col = lerp (0, col, smoothstep (_NoiseFloor - 0.3, _NoiseFloor + 0.3, noise.r));
                    col *= clamp ((noise.r * _NoiseStrength), 0, 1);

                    c *= fixed4 (col, 1);

                    c.rgb *= c.a;
                    return c;
                }

            ENDCG
            }
        }
}
