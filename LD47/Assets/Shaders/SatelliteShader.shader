﻿Shader "Sprites/SatelliteShader"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        
        _NoiseTex ("Noise Texture", 2D) = "white" {}

        _NoiseCutoff ("Noise Cutoff", Range (0, 1)) = 0.5
        _NoiseCutoffWidth ("Noise Cutoff Width", Range (0, 1)) = 0.02
        _AtmosphereScale ("Atmosphere Width", Range (0, 1)) = 0.3
        _Size ("Size", FLOAT) = 0.1
        _Color ("Tint", Color) = (1,1,1,1)
        _SecondaryColour ("Secondary Colour", Color) = (1, 1, 1, 1)
        _AtmosphereColour ("Atmosphere Colour", Color) = (1, 1, 1, 1)

        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

        SubShader
        {
            Tags
            {
                "Queue" = "Transparent"
                "IgnoreProjector" = "True"
                "RenderType" = "Transparent"
                "PreviewType" = "Plane"
                "CanUseSpriteAtlas" = "True"
            }

            Cull Off
            Lighting Off
            ZWrite Off
            Blend One OneMinusSrcAlpha

            Pass
            {
            CGPROGRAM
                #pragma vertex Vert
                #pragma fragment Frag
                #pragma target 2.0
                #pragma multi_compile_instancing
                #pragma multi_compile_local _ PIXELSNAP_ON
                #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
                
                #include "UnityCG.cginc"

                #ifdef UNITY_INSTANCING_ENABLED

                UNITY_INSTANCING_BUFFER_START (PerDrawSprite)
                        // SpriteRenderer.Color while Non-Batched/Instanced.
                        UNITY_DEFINE_INSTANCED_PROP (fixed4, unity_SpriteRendererColorArray)
                        // this could be smaller but that's how bit each entry is regardless of type
                        UNITY_DEFINE_INSTANCED_PROP (fixed2, unity_SpriteFlipArray)
                    UNITY_INSTANCING_BUFFER_END (PerDrawSprite)

                    #define _RendererColor  UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteRendererColorArray)
                    #define _Flip           UNITY_ACCESS_INSTANCED_PROP(PerDrawSprite, unity_SpriteFlipArray)

                #endif // instancing

                CBUFFER_START (UnityPerDrawSprite)
                #ifndef UNITY_INSTANCING_ENABLED
                    fixed4 _RendererColor;
                    fixed2 _Flip;
                #endif
                    float _EnableExternalAlpha;
                CBUFFER_END

                struct appdata_t
                {
                    float4 vertex   : POSITION;
                    float4 color    : COLOR;
                    float2 texcoord : TEXCOORD0;
                    UNITY_VERTEX_INPUT_INSTANCE_ID
                };

                struct v2f
                {
                    float4 vertex   : SV_POSITION;
                    fixed4 color : COLOR;
                    float2 texcoord : TEXCOORD0;
                    float4 localPos : TEXCOORD1;
                    UNITY_VERTEX_OUTPUT_STEREO
                };

                inline float4 UnityFlipSprite (in float3 pos, in fixed2 flip)
                {
                    return float4(pos.xy * flip, pos.z, 1.0);
                }

                v2f Vert (appdata_t IN)
                {
                    v2f OUT;

                    UNITY_SETUP_INSTANCE_ID (IN);
                    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO (OUT);

                    OUT.vertex = UnityFlipSprite (IN.vertex, _Flip);
                    OUT.vertex = UnityObjectToClipPos (OUT.vertex);
                    OUT.texcoord = IN.texcoord;
                    OUT.localPos = UnityFlipSprite (IN.vertex, _Flip);
                    OUT.color = IN.color * _RendererColor;

                    #ifdef PIXELSNAP_ON
                    OUT.vertex = UnityPixelSnap (OUT.vertex);
                    #endif

                    return OUT;
                }

                sampler2D _MainTex;
                sampler2D _AlphaTex;

                fixed4 SampleSpriteTexture (float2 uv)
                {
                    fixed4 color = tex2D (_MainTex, uv);

                #if ETC1_EXTERNAL_ALPHA
                    fixed4 alpha = tex2D (_AlphaTex, uv);
                    color.a = lerp (color.a, alpha.r, _EnableExternalAlpha);
                #endif

                    return color;
                }
                     
                sampler2D _NoiseTex;
                float4 _NoiseTex_ST;

                fixed4 _Color;
                fixed4 _SecondaryColour;
                fixed4 _AtmosphereColour;

                float _Size;
                float _NoiseCutoff;
                float _NoiseCutoffWidth;
                float _AtmosphereScale;

                float smoothStep (float value, float cutoff, float width)
                {
                    return 1 - 1 / (1 + exp ((value - cutoff) / width));
                }

                float smoothBump (float value, float centre, float width)
                {
                    float x = value - centre;
                    return exp (-((x * x / width)));
                }

                fixed4 Frag (v2f IN) : SV_Target
                {
                    fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;

                    fixed4 noise = tex2D (_NoiseTex, IN.texcoord * _NoiseTex_ST.xy + _NoiseTex_ST.zw);
                    
                    fixed4 planetColour = lerp (_Color, _SecondaryColour, smoothStep (noise.r, _NoiseCutoff, _NoiseCutoffWidth));

                    float atmosphereSize = _AtmosphereScale * _Size;
                    float atmospgerePosition = _Size - atmosphereSize;
                    planetColour = lerp (planetColour, _AtmosphereColour, smoothBump (length (IN.localPos.xy) - _Size, atmospgerePosition, atmosphereSize));
                    planetColour = lerp (planetColour, 0, smoothStep (length (IN.localPos.xy) - _Size, atmospgerePosition, atmosphereSize));

                    c *= planetColour;
                    
                    //float r = _Size - length (IN.localPos.xy);
                    //c = lerp (c, 0, step (r, 0));

                    c.rgb *= c.a;
                    return c;
                }

            ENDCG
            }
        }
}
