﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Math = System.Math;

public struct DoubleVector2
{
	public static DoubleVector2 zero { get { return new DoubleVector2 (0.0, 0.0); } }

	public static DoubleVector2 operator + (DoubleVector2 A, DoubleVector2 B)
	{
		return new DoubleVector2 (A.x + B.x, A.y + B.y);
	}

	public static DoubleVector2 operator - (DoubleVector2 A, DoubleVector2 B)
	{
		return new DoubleVector2 (A.x - B.x, A.y - B.y);
	}

	public static DoubleVector2 operator * (double A, DoubleVector2 B)
	{
		return new DoubleVector2 (A * B.x, A * B.y);
	}

	public static DoubleVector2 operator * (DoubleVector2 A, double B)
	{
		return new DoubleVector2 (A.x * B, A.y * B);
	}

	public static DoubleVector2 operator / (DoubleVector2 A, double B)
	{
		return new DoubleVector2 (A.x / B, A.y / B);
	}

	public static explicit operator Vector2 (DoubleVector2 vec)
	{
		return new Vector2 ((float)vec.x, (float)vec.y);
	}

	public static explicit operator DoubleVector2 (Vector2 vec)
	{
		return new DoubleVector2 (vec.x, vec.y);
	}

	public double x;
	public double y;

	public double magnitude
	{
		get
		{
			return Math.Sqrt ((x * x) + (y * y));
		}
	}

	public double sqrMagnitude
	{
		get
		{
			return (x * x) + (y * y);
		}
	}

	public DoubleVector2 normalized
	{
		get
		{
			double mag = magnitude;
			if (mag == 0) return zero;
			return new DoubleVector2 (x / mag, y / mag);
		}
	}

	public DoubleVector2 (double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	public override string ToString ()
	{
		return $"({x.ToString ("f4")},{y.ToString ("f4")})";
	}
}