﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Math = System.Math;

public class FixedSatellite : Satellite
{
    public delegate DoubleVector2 GetPosition (Satellite satellite, double time);

    public static DoubleVector2 FixedCircularOrbit (Satellite centreBody, double time, double radius, bool direction, double periodOffset)
    {
        double angularFrequency = 1 / Math.Sqrt (radius * radius * radius / centreBody.map.gravityConstant / centreBody.mass);
        return centreBody.position + new DoubleVector2 (radius * Math.Cos ((angularFrequency * time) + periodOffset), (direction ? 1 : -1) * radius * Math.Sin ((angularFrequency * time) + periodOffset));
    }

    private GetPosition onGetPosition;

    public FixedSatellite (Map map, double mass, GetPosition onGetPosition, double time)
        : base (map, mass, DoubleVector2.zero, DoubleVector2.zero) 
    {
        this.onGetPosition = onGetPosition;
        position = onGetPosition (this, time);
    }

    public Satellite Free ()
    {
        Satellite newSatellite = map.AddSatellite (this.mass, this.position, this.velocity);
        GameController.Instance.satelliteDisplayController.RemoveDisplay (newSatellite);
        map.RemoveSatellite (this);
        GameController.Instance.satelliteDisplayController.SwapDisplay (this, newSatellite);
        return newSatellite;
    }

    protected override void OnUpdate (double deltaTime, double time)
    {
        DoubleVector2 newPosition = onGetPosition (this, time);
        velocity = (newPosition - position) / deltaTime;
        position = newPosition;
    }
}
