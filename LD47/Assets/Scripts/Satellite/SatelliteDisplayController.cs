﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatelliteDisplayController : MonoBehaviour
{
    public Sprite defaultSprite;

    public Material[] starMaterial;
    public Material[] planetMaterial;

    public Color predictionLineColour = Color.white;
    public Material predictionLineMaterial;

    public double minStarMass = 1;

    private readonly Dictionary<Satellite, SpriteRenderer> currentDisplays = new Dictionary<Satellite, SpriteRenderer> ();
    private readonly Queue<SpriteRenderer> rendererPool = new Queue<SpriteRenderer> ();

    private void LateUpdate ()
    {
        Camera mainCamera = Camera.main;

        Satellite sat;
        Satellite referenceBody;
        DoubleVector2 referencePosition;

        foreach (KeyValuePair<Satellite, SpriteRenderer> item in currentDisplays)
        {
            if (item.Value.gameObject.activeSelf)
            {
                sat = item.Key;
                referenceBody = sat.map.referenceBody;
                referencePosition = sat.map.ReferencePosition;

                item.Value.transform.position = (Vector2)(sat.position - GameController.Instance.map.ReferencePosition);
                item.Value.material.SetFloat ("_Size", (float)sat.size);

                //if (sat is FixedSatellite) item.Value.gameObject.name = $"Fixed Satellite ({(sat.alive ? "Alive" : "Dead")})";
                //else item.Value.gameObject.name = $"Free Satellite ({(sat.alive ? "Alive" : "Dead")})";

                LineRenderer lr = item.Value.GetComponent<LineRenderer> ();
                if (GameController.Instance.predictPaths)
                {
                    lr.enabled = true;
                    lr.startWidth = mainCamera.orthographicSize / 75;
                    lr.endWidth = mainCamera.orthographicSize / 75;
                    lr.positionCount = sat.predictedDataCount;

                    for (int i = 0; i < sat.predictedDataCount; i++)
                    {
                        if (referenceBody != null)
                            lr.SetPosition (i, (Vector2)(sat.GetPredictedPosition (i) - referenceBody.GetPredictedPosition (Mathf.Min (referenceBody.predictedDataCount - 1, i))));
                        else
                            lr.SetPosition (i, (Vector2)(sat.GetPredictedPosition (i) - referencePosition));
                    }
                }
                else
                {
                    lr.enabled = false;
                }
            }
        }
    }

    public void GenerateDisplay (Satellite satellite)
    {
        if (!currentDisplays.ContainsKey (satellite))
        {
            SpriteRenderer sr = getRenderer ();
            if (sr.material != null) Destroy (sr.material);
            sr.material = getRandomMaterial (satellite.mass);
            sr.flipX = Random.value < 0.5f;
            sr.flipY = Random.value < 0.5f;
            sr.material.SetFloat ("_Size", (float)satellite.size);
            sr.material.SetTextureOffset ("_NoiseTex", new Vector2 (Random.Range (0.0f, 1.0f), Random.Range (0.0f, 1.0f)));

            currentDisplays.Add (satellite, sr);
        }
    }

    public void RemoveAll ()
    {
        foreach (KeyValuePair<Satellite, SpriteRenderer> item in currentDisplays)
        {
            item.Value.gameObject.SetActive (false);
            rendererPool.Enqueue (item.Value);
        }
    }

    public void RemoveDisplay (Satellite satellite)
    {
        if (currentDisplays.TryGetValue (satellite, out SpriteRenderer sr))
        {
            sr.gameObject.SetActive (false);
            rendererPool.Enqueue (sr);
        }
    }

    public void ShowDisplay (Satellite satellite)
    {
        if (currentDisplays.TryGetValue (satellite, out SpriteRenderer sr)) sr.gameObject.SetActive (true);
    }

    public void HideDisplay (Satellite satellite)
    {
        if (currentDisplays.TryGetValue (satellite, out SpriteRenderer sr)) sr.gameObject.SetActive (false);
    }

    public void SwapDisplay (Satellite oldSatellite, Satellite newSatellite)
    {
        if (currentDisplays.TryGetValue (oldSatellite, out SpriteRenderer sr))
        {
            currentDisplays.Remove (oldSatellite);
            currentDisplays.Add (newSatellite, sr);
        }
    }

    private Material getRandomMaterial (double mass)
    {
        if (mass > minStarMass)
        {
            return new Material (starMaterial[Random.Range (0, starMaterial.Length)]);
        }
        else
        {
            return new Material (planetMaterial[Random.Range (0, planetMaterial.Length)]);
        }
    }

    private SpriteRenderer getRenderer ()
    {
        if (rendererPool.Count > 0)
        {
            SpriteRenderer sr = rendererPool.Dequeue ();
            sr.gameObject.SetActive (true);

            return sr;
        }
        else
        {
            GameObject go = new GameObject ("SatelliteDisplay");
            go.transform.SetParent (transform);

            SpriteRenderer sr = go.AddComponent<SpriteRenderer> ();
            sr.sprite = defaultSprite;
            sr.sortingOrder = 10;

            LineRenderer lr = go.AddComponent<LineRenderer> ();
            lr.startColor = predictionLineColour;
            lr.endColor = predictionLineColour;
            lr.sharedMaterial = predictionLineMaterial;
            lr.sortingOrder = 5;

            return sr;
        }
    }
}
