﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Math = System.Math;

public abstract class Satellite
{
    public struct Prediction
    {
        public DoubleVector2 position;
        //public DoubleVector2 velocity;
        //public double mass;
        //public bool alive;

        public Prediction (Satellite satellite)
        {
            this.position = satellite.position;
            //this.velocity = satellite.velocity;
            //this.mass = satellite.mass;
            //this.alive = satellite.alive;
        }

        public void Set (Satellite satellite)
        {
            this.position = satellite.position;
            //this.velocity = satellite.velocity;
            //this.mass = satellite.mass;
            //this.alive = satellite.alive;
        }
    }

    public readonly Map map;

    public DoubleVector2 position { get; protected set; }
    public DoubleVector2 velocity { get; protected set; }

    public double size { get; private set; }
    public double mass
    {
        get
        {
            return _mass;
        }
        private set
        {
            //const double r = 100;
            //const double d = 50;

            _mass = value;
            size = Math.Pow (mass * 0.002, 1.0 / 3.0);// + Math.Exp (-((_mass - r) * (_mass - r) / d));
        }
    }
    private double _mass;

    public bool alive { get; private set; }
    public bool isTest { get; private set; }

    protected bool inPrediction { get; private set; }

    private readonly DoubleVector2[] predictedData;
    public int predictedDataCount { get; private set; }

    private DoubleVector2 oldPosition;
    private DoubleVector2 oldVelocity;
    private double oldMass;
    private bool oldAlive;

    public Satellite (Map map, double mass, DoubleVector2 startPosition, DoubleVector2 startVelocity)
    {
        this.map = map;
        this.mass = mass;
        this.position = startPosition;
        this.velocity = startVelocity;

        predictedData = new DoubleVector2[map.predictionSteps];
        predictedDataCount = 0;

        alive = true;
        isTest = false;
        inPrediction = false;
    }

    public void Update (double deltaTime, double time)
    {
        if (alive)
        {
            if (isTest)
            { 
            }

            OnUpdate (deltaTime, time);

            Satellite collidedSatellite = map.DoesCollide (this);
            if (collidedSatellite != null && collidedSatellite != this && this.mass < collidedSatellite.mass)
            {
                alive = false;

                if (!inPrediction && !isTest)
                {
                    collidedSatellite.Absorb (mass, velocity);
                    map.RemoveSatellite (this);
                    GameController.Instance.satelliteDisplayController.RemoveDisplay (this);
                    if (map.referenceBody == this) map.SelectReferenceFrame (collidedSatellite);
                }
            }
            else if (position.sqrMagnitude > map.maxDistance * map.maxDistance)
            {
                alive = false;

                if (!inPrediction && !isTest)
                {
                    GameController.Instance.satelliteDisplayController.HideDisplay (this);
                    position = position.normalized * map.maxDistance;
                    map.SatelliteReachedInfinity (this);
                    map.RemoveSatellite (this);
                    predictedDataCount = 0;
                    if (map.referenceBody == this) map.SelectReferenceFrame (null);
                }
            }

            if (inPrediction)
            {
                if (predictedDataCount < predictedData.Length)
                {
                    predictedData[predictedDataCount] = position;
                    predictedDataCount++;
                }
                //else predictedData.Add (new Prediction (this));
            }
        }
    }

    public DoubleVector2 GetPredictedPosition (int index)
    {
        if (index >= 0 && index < predictedDataCount) return predictedData[index];
        else return DoubleVector2.zero;
    }

    public DoubleVector2 GetAcceleration (DoubleVector2 point)
    {
        DoubleVector2 delta = position - point;
        double distSqrt = delta.sqrMagnitude;
        distSqrt = Math.Max (distSqrt, 0.01);

        return (map.gravityConstant * mass / distSqrt / Math.Sqrt (distSqrt)) * delta;
    }

    public void Relocate (DoubleVector2 startPosition, DoubleVector2 startVelocity)
    {
        this.position = startPosition;
        this.velocity = startVelocity;
    }

    public void SetAsTest ()
    {
        isTest = true;
        alive = true;
    }

    public void GetAcceleration (DoubleVector2 point, out double x, out double y)
    {
        double deltaX = position.x - point.x;
        double deltaY = position.y - point.y;
        double distSqrt = (deltaX * deltaX) + (deltaY * deltaY);
        distSqrt = Math.Max (distSqrt, 0.01);

        double scale = map.gravityConstant * mass / distSqrt / Math.Sqrt (distSqrt);
        x = scale * deltaX;
        y = scale * deltaY;
    }

    public bool IsInside (DoubleVector2 point, double pointSize = 0)
    {
        double totalSize = size + pointSize;
        double deltaX = position.x - point.x;
        double deltaY = position.y - point.y;
        return (deltaX * deltaX) + (deltaY * deltaY) <= totalSize * totalSize;
    }

    public void Absorb (double mass, DoubleVector2 velocity)
    {
        DoubleVector2 momentum = this.mass * this.velocity;
        momentum += mass * velocity;

        this.velocity = momentum / (this.mass + mass);
        this.mass += mass;
    }

    public void Eject (double ejectionMass, double startPositionAngle, DoubleVector2 ejectionVelocity)
    {
        DoubleVector2 startPosition = this.position + new DoubleVector2 ((size + 0.2) * 1.1 * Math.Cos (startPositionAngle), (size + 0.2) * 1.1 * Math.Sin (startPositionAngle));

        if (this.mass > ejectionMass)
        {
            map.AddSatellite (ejectionMass, startPosition, ejectionVelocity + velocity);
            this.mass -= ejectionMass;
        }
        else
        {
            Satellite other = map.AddSatellite (this.mass, startPosition, ejectionVelocity + velocity);

            alive = false;

            if (!inPrediction)
            {
                map.RemoveSatellite (this);
                GameController.Instance.satelliteDisplayController.RemoveDisplay (this);
                if (map.referenceBody == this) map.SelectReferenceFrame (other);
            }
        }
    }

    public void StartPrediction ()
    {
        if (inPrediction) StopPrediction ();
        inPrediction = true;
        predictedDataCount = 0;

        oldPosition = position;
        oldVelocity = velocity;
        oldMass = mass;
        oldAlive = alive;
    }

    public void StopPrediction ()
    {
        if (!inPrediction) return;
        inPrediction = false;

        position = oldPosition;
        velocity = oldVelocity;
        mass = oldMass;
        alive = oldAlive;
    }

    protected abstract void OnUpdate (double deltaTime, double time);
}
