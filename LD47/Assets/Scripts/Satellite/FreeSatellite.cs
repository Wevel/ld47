﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Math = System.Math;

public class FreeSatellite : Satellite
{
    public static DoubleVector2 CircularOrbitPosition (Satellite centreBody, double radius, bool direction, double periodOffset)
    {
        return centreBody.position + new DoubleVector2 (radius * Math.Cos (periodOffset), (direction ? 1 : -1) * radius * Math.Sin (periodOffset));
    }

    public static DoubleVector2 CircularOrbitVelocity (Satellite centreBody,  double radius, bool direction, double periodOffset)
    {
        double speed = Math.Sqrt (centreBody.map.gravityConstant * centreBody.mass / radius);
        return new DoubleVector2 ((direction ? 1 : -1) * speed * Math.Sin (periodOffset), -speed * Math.Cos (periodOffset));
    }

    public FreeSatellite (Map map, double mass, DoubleVector2 startPosition, DoubleVector2 startVelocity)
        : base (map, mass, startPosition, startVelocity) { }

	protected override void OnUpdate (double deltaTime, double time)
	{
		DoubleVector2 acceleration = map.GetAcceleration (position);
		velocity += (acceleration) * deltaTime;
		position += (velocity) * deltaTime;
    }
}
