﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Math = System.Math;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    public Map map { get; private set; }

    public SatelliteDisplayController satelliteDisplayController;
    public CameraController cameraController;
    public LineRenderer infinityLineRenderer;
    public LineRenderer testParticleLineRenderer;

    public Text scoreText;
    public Text massScoreText;
    public GameObject startButton;
    public GameObject reStartButton;
    public GameObject resumeButton;
    public GameObject exitButton;
    public GameObject menueHolder;
    public GameObject scoreHolder;
    public GameObject gameOverRestartButton;
    public GameObject enablePredictionButton;
    public GameObject disablePredictionButton;

    public double gravityConstant = 0.5;
    public double predictionTime = 5;
    public double maxDistance = 100;
    public double minMassForGravity = 0;
    public double timeStep = 0.005f;
    public double predictionTimeStep = 0.02f;

    public int numSatellites;

    public double startPositionAngleChangeRate = 0.5;

    public int infinityLinePointCount = 200;

    private Satellite testSatellite;
    private Satellite originBody;
    private Satellite homePlanet;
    private double startPositionAngle = 0;

    private double ejectMass = 0.5;

    public bool paused = false;

    public bool hasStarted = false;
    public bool predictPaths = false;

    private void OnEnable ()
    {
        if (Instance == null) Instance = this;
        else Debug.LogError ("Already got a game controller");
    }

    private void OnDisable ()
    {
        if (Instance == this) Instance = null;
    }

    private void Start ()
    {
        StartNewGame ();
    }

    private void FixedUpdate2 ()
    {
        if (map != null)
        {
            Camera mainCamera = Camera.main;

            map.Update ();
            numSatellites = map.SatelliteCount;

            if (predictPaths)
            {
                if (testSatellite != null && homePlanet.alive && homePlanet.mass > 0 && hasStarted)
                {
                    const double speedRatio = 0.25;
                    const double offset = 0.2;
                    DoubleVector2 startPosition = homePlanet.position + new DoubleVector2 ((homePlanet.size + offset) * 1.1 * Math.Cos (startPositionAngle), (homePlanet.size + offset) * 1.1 * Math.Sin (startPositionAngle));
                    DoubleVector2 dir = ((DoubleVector2)(Vector2)mainCamera.ScreenToWorldPoint (Input.mousePosition) - (startPosition - map.ReferencePosition));
                    double dist = dir.magnitude;
                    dir /= dist;
                    double orbitalSpeed = Math.Sqrt (map.gravityConstant * homePlanet.mass / homePlanet.size);
                    double escapeSpeed = Math.Sqrt (2.0 * map.gravityConstant * homePlanet.mass / homePlanet.size);
                    double maxInitialSpeed = (speedRatio * orbitalSpeed) + ((1.0 - speedRatio) * escapeSpeed);
                    double initialSpeed = Math.Min (dist, maxInitialSpeed);

                    Debug.DrawRay ((Vector2)(startPosition - map.ReferencePosition), (Vector2)(homePlanet.velocity + (initialSpeed * dir) - map.ReferenceVelocity), Color.red);
                    testSatellite.Relocate (startPosition, homePlanet.velocity + (initialSpeed * dir));
                    testSatellite.SetAsTest ();
                }
                else
                {
                    if (testSatellite != null)
                    {
                        map.RemoveSatellite (testSatellite);
                        testSatellite = null;
                        testParticleLineRenderer.positionCount = 0;
                    }
                }

                map.StartPrediction ();
                map.UpdatePrediction (map.currentTime);

                if (testSatellite != null && homePlanet.mass > 0 && hasStarted)
                {
                    Satellite referenceBody = map.referenceBody;
                    DoubleVector2 referencePosition = map.ReferencePosition;
                    testParticleLineRenderer.positionCount = testSatellite.predictedDataCount;
                    testParticleLineRenderer.startWidth = mainCamera.orthographicSize / 50;
                    testParticleLineRenderer.endWidth = mainCamera.orthographicSize / 50;

                    for (int i = 0; i < testSatellite.predictedDataCount; i++)
                    {
                        if (referenceBody != null)
                            testParticleLineRenderer.SetPosition (i, (Vector2)(testSatellite.GetPredictedPosition (i) - referenceBody.GetPredictedPosition (Mathf.Min (referenceBody.predictedDataCount - 1, i))));
                        else
                            testParticleLineRenderer.SetPosition (i, (Vector2)(testSatellite.GetPredictedPosition (i) - referencePosition));
                    }
                }

                map.StopPrediction ();
            }
        }
    }

    private void Update ()
    {
        if (Input.GetKeyDown (KeyCode.Escape))
        {
            if (hasStarted)
            {
                if (!paused) Pause ();
                else Resume ();
            }
        }

        startButton.SetActive (!hasStarted);
        reStartButton.SetActive (hasStarted);
        resumeButton.SetActive (hasStarted);

#if UNITY_WEBGL
        exitButton.SetActive (false);
#else
        exitButton.SetActive (true);
#endif

        menueHolder.SetActive (paused || !hasStarted);
        scoreHolder.SetActive (hasStarted);
        gameOverRestartButton.SetActive (hasStarted && !paused && homePlanet != null && !homePlanet.alive);

        enablePredictionButton.SetActive (!predictPaths);
        disablePredictionButton.SetActive (predictPaths);

        if (map != null)
        {
            scoreText.text = map.score.ToString ();
            massScoreText.text = map.massScore.ToString ("f1");

            if (!paused) FixedUpdate2 ();

            Camera mainCamera = Camera.main;

            if (hasStarted && !paused)
            {
                if (Input.GetKey (KeyCode.E)) startPositionAngle -= startPositionAngleChangeRate * 2.0 * Math.PI * Time.deltaTime;
                else if (Input.GetKey (KeyCode.Q)) startPositionAngle += startPositionAngleChangeRate * 2.0 * Math.PI * Time.deltaTime;

                if (Input.GetMouseButtonDown (0))
                {
                    if (homePlanet.alive && homePlanet.mass > 0)
                    {
                        const double speedRatio = 0.25;
                        DoubleVector2 startPosition = homePlanet.position + new DoubleVector2 ((homePlanet.size + 0.2) * 1.1 * Math.Cos (startPositionAngle), (homePlanet.size + 0.2) * 1.1 * Math.Sin (startPositionAngle));
                        DoubleVector2 dir = ((DoubleVector2)(Vector2)mainCamera.ScreenToWorldPoint (Input.mousePosition) - (startPosition - map.ReferencePosition)).normalized;
                        double orbitalSpeed = Math.Sqrt (map.gravityConstant * homePlanet.mass / homePlanet.size);
                        double escapeSpeed = Math.Sqrt (2.0 * map.gravityConstant * homePlanet.mass / homePlanet.size);
                        double initialSpeed = (speedRatio * orbitalSpeed) + ((1.0 - speedRatio) * escapeSpeed);
                        homePlanet.Eject (ejectMass, startPositionAngle, initialSpeed * dir);

                        if (!homePlanet.alive) selectReferencePoint (homePlanet.position);
                    }
                }

                if (Input.GetMouseButtonDown (1)) selectReferencePoint ((DoubleVector2)(Vector2)mainCamera.ScreenToWorldPoint (Input.mousePosition));

                if (Input.GetMouseButtonDown (3))
                {
                    List<FixedSatellite> fixedSatellites = new List<FixedSatellite> ();

                    foreach (Satellite item in map.Satellites)
                    {
                        if (item != map.referenceBody && item is FixedSatellite fixedSat) fixedSatellites.Add (fixedSat);
                    }

                    for (int i = 0; i < fixedSatellites.Count; i++) fixedSatellites[i].Free ();
                }
            }

            infinityLineRenderer.startWidth = mainCamera.orthographicSize / 50;
            infinityLineRenderer.endWidth = mainCamera.orthographicSize / 50;
        }
        else
        {
            scoreText.text = "0";
            massScoreText.text = "0.0";
        }
    }

    private void selectReferencePoint (Satellite satellite)
    {
        Vector2 currentPoint = (Vector2)map.ReferencePosition;
        map.SelectReferenceFrame (satellite);
        cameraController.SetLocation (currentPoint - (Vector2)map.ReferencePosition + (Vector2)Camera.main.transform.position);
    }

    private void selectReferencePoint (DoubleVector2 point)
    {
        Vector2 currentPoint = (Vector2)map.ReferencePosition;
        map.SelectReferenceFrame (point);
        cameraController.SetLocation (currentPoint - (Vector2)map.ReferencePosition + (Vector2)Camera.main.transform.position);
    }

    private void OnDrawGizmosSelected ()
    {
        map?.OnDrawGizmosSelected ();
    }

    public void StartNewGame ()
    {
        satelliteDisplayController.RemoveAll ();

        if (hasStarted) map = new Map (timeStep, gravityConstant, maxDistance, predictionTime, predictionTimeStep, minMassForGravity);
        else map = new Map (timeStep, gravityConstant, 1000, predictionTime, predictionTimeStep, minMassForGravity);
        map.Start ();

        originBody = map.AddSatellite (500, (sat, time) => { return new DoubleVector2 (0, 0); });

        if (hasStarted) homePlanet = map.AddSatellite (20, (sat, time) => { return FixedSatellite.FixedCircularOrbit (originBody, time, 12, false, Math.PI * 0.1); });
        else homePlanet = map.AddSatellite (20, originBody, 12, false, Math.PI * 0.1);

        map.AddSatellite (50, originBody, 7, false, Math.PI);
        map.AddSatellite (4, originBody, 16, true, Math.PI * 1.4);
        map.AddSatellite (20, originBody, 25, false, Math.PI * 1.8);
        map.AddSatellite (7, originBody, 30, false, Math.PI * 0.7);


        if (!hasStarted)
        {
            for (int i = 0; i < 20; i++) map.AddSatellite (Random.Range (0.25f, 10f), originBody, Random.Range (5f, 30f), Random.value < 0.5f, Math.PI * Random.Range (0f, 2f));
            selectReferencePoint (originBody);

            cameraController.SetLocation (new Vector2 (10, 0));
            Camera.main.orthographicSize = 15;

            testSatellite = null;

            predictPaths = false;
        }
        else
        {
            testSatellite = map.AddSatellite (ejectMass, DoubleVector2.zero, DoubleVector2.zero, generateDisplay: false);
            testSatellite.SetAsTest ();
            selectReferencePoint (homePlanet);

            predictPaths = true;
        }

        Vector3[] points = new Vector3[infinityLinePointCount];
        float angleStep = 2f * Mathf.PI / points.Length;
        for (int i = 0; i < points.Length; i++) points[i] = new Vector3 ((float)map.maxDistance * Mathf.Cos (angleStep * i), (float)map.maxDistance * Mathf.Sin (angleStep * i), 0);
        infinityLineRenderer.positionCount = points.Length;
        infinityLineRenderer.SetPositions (points);

        paused = false;
    }

    public void StartGame ()
    {
        hasStarted = true;
        StartNewGame ();
    }

    public void Pause ()
    {
        paused = true;
    }

    public void Resume ()
    {
        paused = false;
    }

    public void EnablePrediction ()
    {
        predictPaths = true;
    }

    public void DisEnablePrediction ()
    {
        predictPaths = false;
    }

    public void SpawnStuff ()
    {
        if (map != null) for (int i = 0; i < 5; i++) map.AddSatellite (Random.Range (0.25f, 10f), originBody, Random.Range (5f, 50f), Random.value < 0.5f, Math.PI * Random.Range (0f, 2f));
    }

    public void Exit ()
    {
        Application.Quit ();
    }
}
