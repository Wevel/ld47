﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Math = System.Math;

public class Map
{
    public readonly double gravityConstant;
    public readonly double maxDistance;
    public readonly double minMassForGravity;
    public readonly double timeStep;
    public readonly double predictionTime;
    public readonly double predictionTimeStep;
    public readonly int predictionSteps;
    
    private readonly List<Satellite> satellites = new List<Satellite> ();

    public Satellite referenceBody { get; private set; }

    public double currentTime { get; private set; }
    public int score { get; private set; }
    public double massScore { get; private set; }

    public DoubleVector2 ReferencePosition
    {
        get
        { 
            return referenceBody != null ? referenceBody.position : DoubleVector2.zero;
        }
    }

    public DoubleVector2 ReferenceVelocity
    {
        get
        {
            return referenceBody != null ? referenceBody.velocity : DoubleVector2.zero;
        }
    }

    public IEnumerable<Satellite> Satellites
    {
        get
        {
            for (int i = 0; i < satellites.Count; i++)
            {
                if (satellites[i].alive && !satellites[i].isTest) yield return satellites[i];
            }
        }
    }

    public int SatelliteCount
    {
        get
        {
            return satellites.Count;
        }
    }

    public Map (double timeStep, double gravityConstant, double maxDistance, double predictionTime, double predictionTimeStep, double minMassForGravity)
    {
        this.gravityConstant = gravityConstant;
        this.maxDistance = maxDistance;
        this.predictionTime = predictionTime;
        this.minMassForGravity = minMassForGravity;
        this.timeStep = timeStep;
        this.predictionTimeStep = predictionTimeStep;
        this.predictionSteps = (int)(predictionTime / predictionTimeStep);
    }

    public void Start ()
    {
        currentTime = 0;
        score = 0;
        massScore = 0;
    }

    public void Update ()
    {
        for (int i = satellites.Count - 1; i >= 0; i--) satellites[i].Update (timeStep, currentTime);
        currentTime += timeStep;
    }

    public void OnDrawGizmosSelected ()
    {
        DoubleVector2 referencePosition = ReferencePosition;

        Gizmos.color = Color.red;
        int size = 40;
        double spacing = 0.25f;
        double scale = 0.05f;
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                DoubleVector2 position = (DoubleVector2)(Vector2)Camera.main.transform.position + new DoubleVector2 ((i - (size / 2)) * spacing, (j - (size / 2)) * spacing);
                if (IsInSatellite (position + referencePosition) == null) Gizmos.DrawRay ((Vector2)position, (Vector2)(GetAcceleration (position + referencePosition) * scale));
            }
        }
    }

    public Satellite AddSatellite (double mass, FixedSatellite.GetPosition onGetPosition, bool generateDisplay = true)
    {
        Satellite satellite = new FixedSatellite (this, mass, onGetPosition, currentTime);
        satellites.Add (satellite);
        if (generateDisplay) GameController.Instance.satelliteDisplayController.GenerateDisplay (satellite);
        return satellite;
    }

    public Satellite AddSatellite (double mass, DoubleVector2 position, DoubleVector2 velocity, bool generateDisplay = true)
    {
        Satellite satellite = new FreeSatellite (this, mass, position, velocity);
        satellites.Add (satellite);
        if (generateDisplay) GameController.Instance.satelliteDisplayController.GenerateDisplay (satellite);
        return satellite;
    }

    public Satellite AddSatellite (double mass, Satellite centreBody, double radius, bool direction, double periodOffset, bool generateDisplay = true)
    {
        Satellite satellite = new FreeSatellite (
            this, 
            mass, 
            FreeSatellite.CircularOrbitPosition (centreBody, radius, direction,periodOffset),
            FreeSatellite.CircularOrbitVelocity (centreBody, radius, direction, periodOffset));
        satellites.Add (satellite);
        if (generateDisplay) GameController.Instance.satelliteDisplayController.GenerateDisplay (satellite);
        return satellite;
    }

    public void RemoveSatellite (Satellite satellite)
    {
        if (satellites.Contains (satellite)) satellites.Remove (satellite);
    }

    public void SatelliteReachedInfinity (Satellite satellite)
    {
        Debug.Log ("Reached infinity");
        score++;
        massScore += satellite.mass;
    }

    public DoubleVector2 GetAcceleration (DoubleVector2 point)
    {
        double totalX = 0;
        double totalY = 0;

        double deltaX, deltaY;
        for (int i = 0; i < satellites.Count; i++)
        {
            if (satellites[i].alive && !satellites[i].isTest && satellites[i].mass > minMassForGravity)
            {
                satellites[i].GetAcceleration (point, out deltaX, out deltaY);
                totalX += deltaX;
                totalY += deltaY;
            }
        }
        return new DoubleVector2 (totalX, totalY);
    }

    public Satellite DoesCollide (Satellite satellite)
    {
        for (int i = 0; i < satellites.Count; i++)
        {
            if (satellites[i] != satellite && satellites[i].alive && !satellites[i].isTest && satellites[i].IsInside (satellite.position, satellite.size)) return satellites[i];
        }

        return null;
    }

    public Satellite IsInSatellite (DoubleVector2 point, Satellite excludeSatellite = null)
    {
        for (int i = 0; i < satellites.Count; i++)
        {
            if (satellites[i] != excludeSatellite && satellites[i].alive && !satellites[i].isTest && satellites[i].IsInside (point)) return satellites[i];
        }

        return null;
    }

    public void StartPrediction ()
    {
        for (int i = 0; i < satellites.Count; i++) satellites[i].StartPrediction ();
    }

    public void StopPrediction ()
    {
        for (int i = 0; i < satellites.Count; i++) satellites[i].StopPrediction ();
    }

    public void UpdatePrediction (double time)
    {
        for (int n = 0; n < predictionSteps; n++)
        {
            for (int i = satellites.Count - 1; i >= 0; i--) satellites[i].Update (predictionTimeStep, time);
            time += predictionTimeStep;
        }
    }

    public void SelectReferenceFrame (DoubleVector2 point)
    {
        DoubleVector2 referencePosition = referenceBody != null ? referenceBody.position : DoubleVector2.zero;
        Satellite satellite = IsInSatellite (point + referencePosition);
        //if (satellite != null) 
            SelectReferenceFrame (satellite);
    }

    public void SelectReferenceFrame (Satellite satellite)
    {
        referenceBody = satellite;
    }
}
